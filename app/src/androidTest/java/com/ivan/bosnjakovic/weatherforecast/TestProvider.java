package com.ivan.bosnjakovic.weatherforecast;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.ivan.bosnjakovic.weatherforecast.db.CitesContract;

/**
 * Tests for ContentProvider, affects actual data in db
 */

public class TestProvider extends AndroidTestCase {

    public void testDeleteAllRecordsFromProvider() {
        mContext.getContentResolver().delete(
                CitesContract.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = mContext.getContentResolver().query(
                CitesContract.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records not deleted from table during delete", 0, cursor.getCount());
        cursor.close();
    }

    public void testInsert() {

        ContentValues contentValues = new ContentValues();
        contentValues.put(CitesContract.Columns.CITY_NAME, "one");

        Uri uri = mContext.getContentResolver().insert(CitesContract.CONTENT_URI, contentValues);

        assertTrue("Failed becouse uri is null", uri != null);

    }

    public void testQuery() {
        Cursor cursor = mContext.getContentResolver().query(
                CitesContract.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertTrue("Failed becouse uri is null", cursor.moveToFirst());

    }
}
