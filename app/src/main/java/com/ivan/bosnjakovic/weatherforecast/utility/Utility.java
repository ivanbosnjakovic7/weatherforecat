package com.ivan.bosnjakovic.weatherforecast.utility;

import android.content.Context;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.ivan.bosnjakovic.weatherforecast.R;
import com.ivan.bosnjakovic.weatherforecast.db.CitesContract;
import com.ivan.bosnjakovic.weatherforecast.model.WeatherForecast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Utility {

    /**
     * Helper method to provide the icon resource id according to the weather condition id returned
     * by the OpenWeatherMap call.
     */
    public static int getIconResourceForWeatherCondition(int weatherId) {
        // Based on weather code data found at:
        // https://openweathermap.org/weather-conditions
        if (weatherId >= 200 && weatherId <= 232) {
            return R.drawable.ic_icons8_storm;
        } else if (weatherId >= 300 && weatherId <= 321) {
            return R.drawable.ic_icons8_rain_cloud;
        } else if (weatherId >= 500 && weatherId <= 504) {
            return R.drawable.ic_icons8_rain;
        } else if (weatherId == 511) {
            return R.drawable.ic_icons8_winter;
        } else if (weatherId >= 520 && weatherId <= 531) {
            return R.drawable.ic_icons8_rain;
        } else if (weatherId >= 600 && weatherId <= 622) {
            return R.drawable.ic_icons8_winter;
        } else if (weatherId >= 701 && weatherId <= 761) {
            return R.drawable.ic_icons8_fog;
        } else if (weatherId == 761 || weatherId == 781) {
            return R.drawable.ic_icons8_storm;
        } else if (weatherId == 800) {
            return R.drawable.ic_icons8_summer;
        } else if (weatherId == 801) {
            return R.drawable.ic_icons8_rain_cloud;
        } else if (weatherId >= 802 && weatherId <= 804) {
            return R.drawable.ic_icons8_rain_cloud;
        }
        return -1;
    }

    public static String getLocationFromLaLng(Context context, double lat, double lng) {
        String address = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> adresses = geocoder.getFromLocation(lat, lng, 1);
            for (int i = 0; i < adresses.size(); i++) {
                address += adresses.get(0).getLocality() + "," + adresses.get(0).getCountryCode();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    public static boolean hasCityName(List<WeatherForecast> weatherForecasts) {

        if (weatherForecasts == null) {
            return false;
        }

        if (weatherForecasts.get(0) == null) {
            return false;
        }

        if (TextUtils.isEmpty(weatherForecasts.get(0).getCityName())) {
            return false;
        }

        return true;
    }

    public static List<String> getListOfCitiesFromCursor(Cursor cities) {

        List<String> citiesList = new ArrayList<>();

        cities.moveToFirst();
        while (!cities.isAfterLast()) {
            citiesList.add(cities.getString(cities.getColumnIndex(CitesContract.Columns.CITY_NAME)));
            cities.moveToNext();
        }

        return citiesList;
    }

}
