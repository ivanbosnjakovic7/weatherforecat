package com.ivan.bosnjakovic.weatherforecast.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class AppProvider extends ContentProvider {

    private AppDatabase mOpenHelper;

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    //two constants that represents two kinds of data that our table can give, single or multiple rows of data
    static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "." + CitesContract.CONTENT_AUTHORITY + "." + CitesContract.TABLE_NAME;
    static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "." + CitesContract.CONTENT_AUTHORITY + "." + CitesContract.TABLE_NAME;

    private static final int CITIES = 100;
    private static final int CITY_ID = 101;

    public AppProvider() {
    }

    private static UriMatcher buildUriMatcher() {
        //if there is no table to match, then matcher will return no match constant
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        //  eg. content://com.bosnjakovic.ivan.provider/Cities
        matcher.addURI(CitesContract.CONTENT_AUTHORITY, CitesContract.TABLE_NAME, CITIES);
        // e.g. content://com.bosnjakovic.ivan.provider/Cities/8
        matcher.addURI(CitesContract.CONTENT_AUTHORITY, CitesContract.TABLE_NAME + "/#", CITY_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = AppDatabase.getInstance(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        final SQLiteDatabase db;

        int count;

        String selectionCriteria;

        switch (match) {
            case CITIES:
                db = mOpenHelper.getWritableDatabase();
                count = db.delete(CitesContract.TABLE_NAME, selection, selectionArgs);
                break;

            //this case is called if only one row needs to be updated
            case CITY_ID:
                db = mOpenHelper.getWritableDatabase();
                long cityId = CitesContract.getCityId(uri);
                selectionCriteria = CitesContract.Columns._ID + " = " + cityId;
                if ((selection != null) && (selection.length() > 0)) {
                    selectionCriteria += " AND (" + selection + ")";
                }
                count = db.delete(CitesContract.TABLE_NAME, selectionCriteria, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        return count;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CITIES:
                return CONTENT_TYPE;
            case CITY_ID:
                return CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int match = sUriMatcher.match(uri);
        final SQLiteDatabase db;

        Uri returningUri;
        long recordId;

        switch (match) {
            case CITIES:
                db = mOpenHelper.getWritableDatabase();
                recordId = db.insert(CitesContract.TABLE_NAME, null, values);
                if (recordId >= 0) {
                    returningUri = CitesContract.buildCityUri(recordId);
                } else {
                    throw new SQLException("Failed to insert into " + uri.toString());
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        return returningUri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        final int match = sUriMatcher.match(uri);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (match) {
            case CITIES:
                queryBuilder.setTables(CitesContract.TABLE_NAME);
                break;

            case CITY_ID:
                queryBuilder.setTables(CitesContract.TABLE_NAME);
                long cityId = CitesContract.getCityId(uri);
                queryBuilder.appendWhere(CitesContract.Columns._ID + " = " + cityId);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);

        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Update not available");
    }
}
