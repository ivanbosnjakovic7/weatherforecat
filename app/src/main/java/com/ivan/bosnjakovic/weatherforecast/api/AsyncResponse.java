package com.ivan.bosnjakovic.weatherforecast.api;

import com.ivan.bosnjakovic.weatherforecast.model.WeatherForecast;

import java.util.List;

public interface AsyncResponse {

    void processFinish(List<WeatherForecast> weatherForecasts);
}
