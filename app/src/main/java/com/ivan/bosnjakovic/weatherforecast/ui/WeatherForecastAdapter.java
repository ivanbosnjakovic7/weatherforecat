package com.ivan.bosnjakovic.weatherforecast.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivan.bosnjakovic.weatherforecast.R;
import com.ivan.bosnjakovic.weatherforecast.model.WeatherForecast;
import com.ivan.bosnjakovic.weatherforecast.utility.Utility;

import java.util.List;

public class WeatherForecastAdapter extends RecyclerView.Adapter<ForecastAdapterViewHolder> {

    private Context mContext;
    private List<WeatherForecast> mWeatherForecasts;

    public WeatherForecastAdapter(Context context, List<WeatherForecast> weatherForecasts) {
        this.mContext = context;
        this.mWeatherForecasts = weatherForecasts;
    }

    @NonNull
    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.forecast_item, parent, false);
        return new ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastAdapterViewHolder holder, int position) {

        String description = mWeatherForecasts.get(position).getWeatherDescription() + "\n" + mWeatherForecasts.get(position).getLongerDescription();

        int iconId = Utility.getIconResourceForWeatherCondition(mWeatherForecasts.get(position).getWeatherId());

        holder.mIconView.setImageResource(iconId);
        holder.mLowTempView.setText(String.valueOf(mWeatherForecasts.get(position).getMinTemp()));
        holder.mDescriptionView.setText(description);
        holder.mHighTempView.setText(String.valueOf(mWeatherForecasts.get(position).getMaxTemp()));
    }

    @Override
    public int getItemCount() {
        if (mWeatherForecasts != null) {
            return mWeatherForecasts.size();
        }

        return -1;
    }

}
