package com.ivan.bosnjakovic.weatherforecast.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Cities.db";
    public static final int DATABASE_VERSION = 1;
    private static final String ON_CREATE = "ON CREATE";
    private static final String ON_UPGRADE = "ON UPGRADE";

    // Implement AppDatabase as a Singleton
    private static AppDatabase database = null;

    private AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Get an database of the app's singleton database helper object
     * Method that returns single instance of AppDatabase object -- singleton pattern
     *
     * @param context the content providers context.
     * @return a SQLite database helper object
     */
    public static AppDatabase getInstance(Context context) {
        if (database == null) {
            database = new AppDatabase(context);
        }
        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(ON_CREATE, "onCreate: starts");
        String sSQL;

        sSQL = "CREATE TABLE " + CitesContract.TABLE_NAME + " ("
                + CitesContract.Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
                + CitesContract.Columns.CITY_NAME + " TEXT NOT NULL);";
        Log.d(ON_CREATE, sSQL);
        db.execSQL(sSQL);

        Log.d(ON_CREATE, "onCreate: ends");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(ON_UPGRADE, "onUpgrade: starts");
        switch (oldVersion) {
            case 1:
                // upgrade logic from version 1
                break;
            default:
                throw new IllegalStateException("onUpgrade() with unknown newVersion: " + newVersion);
        }
        Log.d(ON_UPGRADE, "onUpgrade: ends");
    }

}
