package com.ivan.bosnjakovic.weatherforecast;

import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.ivan.bosnjakovic.weatherforecast.api.AsyncResponse;
import com.ivan.bosnjakovic.weatherforecast.api.FetchData;
import com.ivan.bosnjakovic.weatherforecast.model.WeatherForecast;
import com.ivan.bosnjakovic.weatherforecast.ui.WeatherForecastAdapter;
import com.ivan.bosnjakovic.weatherforecast.ui.dialogs.AddLocationDialog;
import com.ivan.bosnjakovic.weatherforecast.ui.dialogs.OnCityChanged;
import com.ivan.bosnjakovic.weatherforecast.utility.Utility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastActivity extends AppCompatActivity implements AsyncResponse, LocationListener, OnCityChanged {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final String ADD_LOCATION_DIALOG_TAG = "ADD_L_TAG";
    private static final long MIN_TIME = 400L;
    private static final float MIN_DISTANCE = 1f;

    @BindView(R.id.recyclerview_forecast)
    RecyclerView recyclerView;
    @BindView(R.id.tvCity)
    TextView tvCity;

    private LocationManager locationManager;
    private FetchData fetchData;
    private AddLocationDialog addLocationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        init();
        requestUsersLocationPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(getProvider(), MIN_TIME, MIN_DISTANCE, this);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    //Remove the locationlistener updates when Activity is paused
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.miAddLocation:
                showAddLocationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAddLocationDialog() {
        addLocationDialog = new AddLocationDialog();
        addLocationDialog.setOnDialogCitySelected(this);
        addLocationDialog.show(getSupportFragmentManager(), ADD_LOCATION_DIALOG_TAG);
    }

    @Override
    public void processFinish(List<WeatherForecast> weatherForecasts) {
        WeatherForecastAdapter forecastAdapter = new WeatherForecastAdapter(this, weatherForecasts);
        recyclerView.setAdapter(forecastAdapter);

        String cityNameText = Utility.hasCityName(weatherForecasts) ? getString(R.string.forecast_city_text) + weatherForecasts.get(0).getCityName() : getString(R.string.no_data_text);

        tvCity.setText(cityNameText);
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocation();
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        String currentAddress = Utility.getLocationFromLaLng(this, lat, lng);

        fetchData(currentAddress);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    private void init() {
        initLayout();
        initLocationManager();
    }

    private void initLocationManager() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    private void initLayout() {
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void fetchData(String address) {
        fetchData = new FetchData();
        fetchData.setDelegate(this);
        fetchData.execute(address);
    }

    private void requestUsersLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            checkLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void checkLocation() throws SecurityException {

        Location location = locationManager.getLastKnownLocation(getProvider());

        if (location != null) {
            onLocationChanged(location);
        }

    }

    private String getProvider() {
        return locationManager.getBestProvider(new Criteria(), false);
    }

    @Override
    public void onCityChanged(String city) {
        fetchData(city);
    }
}
