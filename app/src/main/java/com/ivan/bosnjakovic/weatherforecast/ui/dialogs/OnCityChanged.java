package com.ivan.bosnjakovic.weatherforecast.ui.dialogs;

public interface OnCityChanged {

    void onCityChanged(String city);

}
