package com.ivan.bosnjakovic.weatherforecast.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivan.bosnjakovic.weatherforecast.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.list_item_icon)
    ImageView mIconView;
    @BindView(R.id.list_item_forecast_textview)
    TextView mDescriptionView;
    @BindView(R.id.list_item_high_textview)
    TextView mHighTempView;
    @BindView(R.id.list_item_low_textview)
    TextView mLowTempView;

    public ForecastAdapterViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
