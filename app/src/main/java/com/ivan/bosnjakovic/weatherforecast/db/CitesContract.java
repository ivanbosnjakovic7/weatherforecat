package com.ivan.bosnjakovic.weatherforecast.db;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class CitesContract {

    //name of table in db
    public static final String TABLE_NAME = "Cities";

    public static class Columns {
        public static final String _ID = BaseColumns._ID;
        public static final String CITY_NAME = "City_name";

        private Columns() {
            // private constructor to prevent instantiation
        }
    }

    //name of content provider, unique authority that must match authority in android manifest
    static final String CONTENT_AUTHORITY = "com.bosnjakovic.ivan.provider";
    //public uri constant that is used later on when we are accessing and manipulating data, it is a scheme that must start with content
    //more on schemes and components on: https://tools.ietf.org/html/rfc3986#section-3.1
    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * The URI to access the Cities table
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME);

    //method to create uri for given id
    static Uri buildCityUri(long cityId) {
        return ContentUris.withAppendedId(CONTENT_URI, cityId);
    }

    //method to retrive id from given uri
    static long getCityId(Uri uri) {
        return ContentUris.parseId(uri);
    }

}
