package com.ivan.bosnjakovic.weatherforecast.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ivan.bosnjakovic.weatherforecast.R;
import com.ivan.bosnjakovic.weatherforecast.db.CitesContract;

import com.ivan.bosnjakovic.weatherforecast.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ivan.bosnjakovic.weatherforecast.db.CitesContract.CONTENT_URI;

public class AddLocationDialog extends DialogFragment implements DialogInterface.OnClickListener {

    @BindView(R.id.etCityInput)
    EditText etInputCity;
    @BindView(R.id.tvChooseCity)
    TextView tvChooseCity;
    @BindView(R.id.lvCities)
    ListView lvCities;
    @BindView(R.id.bAdd)
    Button bAdd;

    private OnCityChanged onDialogCitySelected;

    public void setOnDialogCitySelected(OnCityChanged onDialogCitySelected) {
        this.onDialogCitySelected = onDialogCitySelected;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.add_location_dialog, null);

        ButterKnife.bind(this, dialogView);

        builder.setIcon(R.drawable.round_add_location_black_24);
        builder.setTitle(R.string.add_location_dialog_title);
        builder.setView(dialogView);
        builder.setNegativeButton(R.string.cancel_button_text, this);

        loadList();
        setupListeners();

        return builder.create();

    }

    private void setupListeners() {

        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = etInputCity.getText().toString();

                if (validateInput(input)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CitesContract.Columns.CITY_NAME, input);
                    getActivity().getContentResolver().insert(CONTENT_URI, contentValues);
                    etInputCity.setText("");
                } else {
                    etInputCity.setError(getString(R.string.add_valid_city_message));
                }

                loadList();
            }
        });

        lvCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String city = lvCities.getItemAtPosition(i).toString();
                onDialogCitySelected.onCityChanged(city);
            }
        });

        lvCities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int id, long l) {
                String selection = CitesContract.Columns.CITY_NAME + " = ?";
                String[] selArgs = new String[]{lvCities.getItemAtPosition(id).toString()};
                getActivity().getContentResolver().delete(CONTENT_URI, selection, selArgs);
                loadList();
                return true;
            }
        });
    }

    private void loadList() {

        Cursor result = getActivity().getContentResolver().query(CONTENT_URI, null, null, null, null);
        List<String> citiesFromCursor = new ArrayList<>();

        citiesFromCursor.addAll(Utility.getListOfCitiesFromCursor(result));

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, citiesFromCursor);

        lvCities.setAdapter(arrayAdapter);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }

    private boolean validateInput(String input) {

        if (TextUtils.isEmpty(input)) {
            return false;
        }

        return true;
    }
}
