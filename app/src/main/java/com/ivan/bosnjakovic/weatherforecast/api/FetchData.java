package com.ivan.bosnjakovic.weatherforecast.api;

import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.ivan.bosnjakovic.weatherforecast.model.WeatherForecast;
import com.ivan.bosnjakovic.weatherforecast.utility.ResponseKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static com.ivan.bosnjakovic.weatherforecast.utility.ResponseKeys.*;

public class FetchData extends AsyncTask<String, Void, List<WeatherForecast>> {

    private static final String MALFORMED_URL_TAG = "malformed url";
    private static final String MAKE_HTTP_REQUEST_TAG = "http request";
    private static final String NOT_CONNECTED_TAG = "not connected";

    private final String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";

    private final String QUERY_PARAM = "q";
    private final String UNITS_PARAM = "units";
    private final String DAYS_PARAM = "cnt";
    private final String APPID_PARAM = "appid";
    private final String APPID_VALUE = "6017cad5639f35795f21f15be367e79a";

    private AsyncResponse delegate;

    public void setDelegate(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected List<WeatherForecast> doInBackground(String... cities) {

        final String units = "metric";
        final int numDays = 14;

        final String city = cities[0];

        Uri.Builder uriBuilder = Uri.parse(BASE_URL).buildUpon();

        Uri builtUri = uriBuilder
                .appendQueryParameter(QUERY_PARAM, city)
                .appendQueryParameter(DAYS_PARAM, String.valueOf(numDays))
                .appendQueryParameter(UNITS_PARAM, units)
                .appendQueryParameter(APPID_PARAM, APPID_VALUE)
                .build();

        URL connectingUrl = createURl(builtUri);

        String response = makeHttpRequest(connectingUrl);

        List<WeatherForecast> weatherForecasts = null;
        try {
            weatherForecasts = getForecastFromResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return weatherForecasts;
    }

    private List<WeatherForecast> getForecastFromResponse(String response) throws JSONException {

        if (TextUtils.isEmpty(response)) {
            return null;
        }

        JSONObject forecastJson = new JSONObject(response);

        JSONArray weatherForecatArray = forecastJson.getJSONArray(ResponseKeys.OWM_LIST);

        JSONObject cityJson = forecastJson.getJSONObject(ResponseKeys.OWM_CITY);
        String cityName = cityJson.getString(ResponseKeys.OWM_CITY_NAME);

        List<WeatherForecast> weatherForecasts = new ArrayList<>();

        for (int i = 0; i < weatherForecatArray.length(); i++) {
            JSONObject oneDayForecast = weatherForecatArray.getJSONObject(i);

            JSONObject weatherObject =
                    oneDayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);

            String description = weatherObject.getString(OWM_DESCRIPTION);
            String longerDescription = weatherObject.getString(OWM_LONG_DESCRIPTION);
            int weatherId = weatherObject.getInt(OWM_WEATHER_ID);

            JSONObject temperatureObject = oneDayForecast.getJSONObject(OWM_TEMPERATURE);
            int maxTemp = temperatureObject.getInt(OWM_MAX);
            int minTemp = temperatureObject.getInt(OWM_MIN);

            weatherForecasts.add(new WeatherForecast(minTemp, maxTemp, description, longerDescription, cityName, weatherId));
        }

        return weatherForecasts;
    }

    private String makeHttpRequest(URL connectingUrl) {

        String jsonResponse = "";

        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        try {
            httpURLConnection = (HttpURLConnection) connectingUrl.openConnection();
            httpURLConnection.setReadTimeout(2000);
            httpURLConnection.setConnectTimeout(2000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = httpURLConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.d(NOT_CONNECTED_TAG, "makeHttpRequest: " + httpURLConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.d(MAKE_HTTP_REQUEST_TAG, "makeHttpRequest: ", e);
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return jsonResponse;
    }

    private String readFromStream(InputStream inputStream) throws IOException {

        StringBuilder output = new StringBuilder();

        if (inputStream != null) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private URL createURl(Uri uriToConvert) {

        try {
            return new URL(uriToConvert.toString());
        } catch (MalformedURLException e) {
            Log.e(MALFORMED_URL_TAG, "createURl failed: ", e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<WeatherForecast> weatherForecasts) {
        if (delegate != null) {
            delegate.processFinish(weatherForecasts);
        }
    }
}
