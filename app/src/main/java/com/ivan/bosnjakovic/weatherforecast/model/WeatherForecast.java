package com.ivan.bosnjakovic.weatherforecast.model;

//url:http://api.openweathermap.org/data/2.5/forecast/daily?q=Zagreb&cnt=7&units=metric&appid=6017cad5639f35795f21f15be367e79a

public class WeatherForecast {

    private int minTemp;
    private int maxTemp;
    private String weatherDescription;
    private String longerDescription;
    private String cityName;
    private int weatherId;

    public WeatherForecast(int minTemp, int maxTemp, String weatherDescription, String longerDescription, String cityName, int weatherId) {
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.weatherDescription = weatherDescription;
        this.longerDescription = longerDescription;
        this.cityName = cityName;
        this.weatherId = weatherId;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getLongerDescription() {
        return longerDescription;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public String getCityName() {
        return cityName;
    }

}
