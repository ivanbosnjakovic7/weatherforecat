package com.ivan.bosnjakovic.weatherforecast.utility;

public class ResponseKeys {

    // Location information
    public final static String OWM_CITY = "city";
    public final static String OWM_CITY_NAME = "name";

    // Weather information.  Each day's forecast info is an element of the "list" array.
    public final static String OWM_LIST = "list";

    // All temperatures are children of the "temp" object.
    public final static String OWM_TEMPERATURE = "temp";
    public final static String OWM_MAX = "max";
    public final static String OWM_MIN = "min";

    public final static String OWM_WEATHER = "weather";
    public final static String OWM_DESCRIPTION = "main";
    public final static String OWM_LONG_DESCRIPTION = "description";
    public final static String OWM_WEATHER_ID = "id";

}
